var ChatApp = React.createClass({
    getInitialState: function() {
        return {
            messages: [],
            firstMessage: true,
            name: " ",
            socket: window.io('http://localhost:3000')
        }
    },
    componentDidMount: function() {
        var self = this;
        this.state.socket.on("receive-message", function(msg){
            var messages = self.state.messages;
            messages.push(msg);
            console.log(self.state.messages);
            self.setState({messages: messages});         
        });
    },
    submitMessage: function() {
        var message = document.getElementById("message").value;
        var name = this.state.name;
        this.state.socket.emit("new-message", name + ": " + message);
        console.log("Message sended to server: " + message);
        document.getElementById("message").value = " ";
    },
    submitUser: function() {
        var name = this.state.name;
        this.state.socket.emit("new-message", "NEW CHAT USER: " + name);
    },
    setName: function() {
        var message = document.getElementById("message").value;
        this.state.name = message;
        this.state.firstMessage = false;
        document.getElementById("message").value = " ";
        {this.submitUser()}
    },
    submit: function() {
        if (this.state.firstMessage !== true){
            {this.submitMessage()}
        } else {
            {this.setName()}
        }
    },
    enterPress: function(e) {
        if (e.charCode === 13) {
            {this.submit()}
        }
    },
    render: function() {
        var self = this;
        var messages = this.state.messages.map(function(msg) {
            return (
                <li>
                {msg}
                </li>
            )
        })
        return (     
            <div>
				<ul>
                   {messages}
                </ul>
                <div className="inputContainer">
                <input id="message" type="text" placeholder="Please type your name:" onKeyPress={() => self.enterPress(event)}/><button id="send" onClick={() => self.submit()}>Send</button>
                </div>
            </div>
        )
    }
});

ReactDOM.render(
    <ChatApp />,
    document.getElementById('container')
);